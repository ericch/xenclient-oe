DESCRIPTION = "Display Manager"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=f1e379c1d5588582790922b24502f0b8"
DEPENDS = "dbus qtbase libivc2 libpvbackendhelper vglass"
RDEPENDS_${PN} += "dbus qtbase libivc2 libpvbackendhelper vglass bash"
# INSANE_SKIP_${PN} += "dev-deps"
SRCREV = "${AUTOREV}"
PV = "0+git${SRCPV}"
SRC_URI = "git://gitlab.com/vglass/disman.git;protocol=ssh;branch=${OPENXT_BRANCH} \
           file://cpp-config.patch \
"
inherit qmake5
inherit update-rc.d
INITSCRIPT_NAME = "disman"
INITSCRIPT_PARAMS = "defaults 99 0"
S = "${WORKDIR}/git"
export SVLIB_PRODUCTION
do_configure_prepend() {
    echo 'INCLUDEPATH += ${PKG_CONFIG_SYSROOT_DIR}${includedir}/toolstack/xenmgr' >> ${S}/disman.pro
    echo 'INCLUDEPATH += ${PKG_CONFIG_SYSROOT_DIR}${includedir}/toolstack/include' >> ${S}/disman.pro
    echo 'INCLUDEPATH += ${PKG_CONFIG_SYSROOT_DIR}${includedir}/common/include' >> ${S}/disman.pro
    echo 'INCLUDEPATH += ${PKG_CONFIG_SYSROOT_DIR}${includedir}/common/include/gsl' >> ${S}/disman.pro 
}
