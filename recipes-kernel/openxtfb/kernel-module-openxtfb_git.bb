# Copyright (C) 2015 Assured Information Security, Inc.
#
# Released under the MIT license (see COPYING.MIT for the terms)
SUMMARY = "Framebuffer Driver for OpenXT Linux Guests"
DESCRIPTION = " \
    Simple framebuffer driver for OpenXT Linux Guests using \
    the new Display Handler. Supports single or multiple framebuffers, \
    which can provide simple single-monitor or multi-monitor support \
    (when used with a software renderer, like Xinerama). \
"
AUTHOR = "Kyle J. Temkin <temkink@ainfosec.com>"
HOMEPAGE = "http://www.ainfosec.com"
SECTION  = "kernel/modules"
PR = "3"
#Allow openxtfb to be used by its original, non-OE name.
#This limits the scope of this file's changes in the DH layer.
PROVIDES = "openxtfb"
RPROVIDES_${PN} = "openxtfb"
KERNEL_MODULE = "openxtfb"
KERNEL_MODULE_PACKAGE_SUFFIX = ""
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b93b8e559b6be52894479d52aa1ccb6c"
#Ensure that we have the PV display helper module, 
#and that Kbuild can build using it.
DEPENDS = "kernel-module-pv-display-helper"
RDEPENDS_${PN} = "kernel-module-pv-display-helper"
MODULE_DEPENDS = "pv_display_helper ivc"
#TODO: Fix the unnecessary IVC dependency in pv_display_helper,
#and then get rid of the IVC entry.
#Use the current working state. Once this pull request is merged, we can switch
#to using the DH variant.
SRC_URI = "git://gitlab.com/vglass/openxtfb.git;protocol=ssh;branch=${OPENXT_BRANCH} \
    file://compiler.patch \
"
#Always grab the latest from git.
SRCREV = "${AUTOREV}"
PV = "0+git${SRCPV}"
#... and built it directly in the git export directory.
S = "${WORKDIR}/git"
inherit oxt-module
#
# We'll use make directly to create the module-- it will, in turn, call
# Kbuild.
#
do_compile () {
    oe_runmake
}
#
# For now, we'll install the kernel module manually, rather than using the kernel
# modules_install. Later, it may be desireable to get this down to something that
# can be done just by calling "modules_install".
#
do_install () {
    oe_runmake INSTALL_MOD_PATH="${D}" modules_install
    #For now, we'll manually install the relevant userspace headers into the
    #target include path.
    install -d "${D}${includedir}/linux"
    install -m 0644 "${S}/openxtfb.h" "${D}${includedir}/linux/openxtfb.h"
}
